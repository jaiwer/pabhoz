<?php
/*Base para un proyecto MVC en PHP
*Orientada a objetos (Mapeo Objetos Relacionales) ORM

*@autor: jaiwer
*Descripcion de Index_controler 
*/

class Index_controller {
	public function index($param=null){
		if(empty($param)){
			$param = "Tu";
		}
		echo "Hola ".$param."!";
	}
}